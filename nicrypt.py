import string
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-e", "--encrypt", help="Encrypt message", action="store_true")
parser.add_argument("-d", "--decrypt", help="Decrypt message", action="store_true")
parser.add_argument("-B", "--body", help="The body that should be dealt with", type=str)
parser.add_argument("-S", "--shift", help="Shift number [ex: -S 3,6,12 ]", type=str)
args=parser.parse_args()


abc=list(string.ascii_lowercase)
abc.append(" ")

def encrypt_dictionary(text, shift):
    encrypted = list()
    text=text.lower()
    index = 0
    for char in text:
        char_index = abc.index(char) + shift[index%len(shift)] # real and alt
        encrypted.append(abc[char_index%len(abc)])
        index+=1
    return "".join(encrypted)

def encrypt_message(message, shift, dct="".join(abc)):
    dictionary=encrypt_dictionary(dct, shift)
    encrypted_message = list()
    for char in message.lower():
        encrypted_message.append(dictionary[dct.index(char)])
    return "".join(encrypted_message)

# yeah I could refactor, but who cares :)
def decrypt_message(message, shift, dct="".join(abc)):
    dictionary=encrypt_dictionary(dct, shift)
    decrypted_message = list()
    for char in message.lower():
        decrypted_message.append(dct[dictionary.index(char)])
    return "".join(decrypted_message)


if args.body == None or args.shift == None:
    print("You must provide a shift and body, run the command with -h")

shift=[int(s) for s in args.shift.split(",")]
if args.encrypt:
    print(f"ENCRYPTED MESSAGE:\t{encrypt_message(args.body, shift)}")
elif args.decrypt:
    print(f"DECRYPTED MESSAGE:\t{decrypt_message(args.body, shift)}")
else:
    print("you must choose a method [-e/-d]")
